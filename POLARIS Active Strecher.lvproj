﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Folder">
			<Item Name="FSU Bahrmann SMS" Type="Folder">
				<Item Name="ftd2xx.dll" Type="Document" URL="../Submodules/HIJ Motion/Bahrmann Utility/ftd2xx.dll"/>
				<Item Name="SMS_DEMO_V4.5.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_V4.5.vi"/>
				<Item Name="USB_INFO.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_INFO.vi"/>
				<Item Name="USB_INIT.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_INIT.vi"/>
				<Item Name="USB_NET_CMD_SMS.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/USB_NET_CMD_SMS.vi"/>
				<Item Name="USB_NET_ECMD.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_NET_ECMD.vi"/>
				<Item Name="USB_NET_IO.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_NET_IO.vi"/>
				<Item Name="USB_NET_RW_SMS.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/USB_NET_RW_SMS.vi"/>
			</Item>
			<Item Name="Active Stretcher Motors.ico" Type="Document" URL="../Active Stretcher Motors/Active Stretcher Motors/Active Stretcher Motors.ico"/>
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="POLARIS Open G.lvlib" Type="Library" URL="../Submodules/POLARIS OpenG/POLARIS Open G.lvlib"/>
			<Item Name="ViewableActor.lvlib" Type="Library" URL="../Submodules/Viewable Actor/ViewableActor.lvlib"/>
		</Item>
		<Item Name="HIJ Motion" Type="Folder">
			<Item Name="Controller" Type="Folder">
				<Item Name="Error Code 2 Error Cluster.vi" Type="VI" URL="../Submodules/HIJ Motion/Error Handling/Error Code 2 Error Cluster.vi"/>
				<Item Name="FSU SMS Actor.lvlib" Type="Library" URL="../Submodules/HIJ Motion/FSU SMS Actor/FSU SMS Actor.lvlib"/>
				<Item Name="FSU SMS Errors.ctl" Type="VI" URL="../Submodules/HIJ Motion/Error Handling/FSU SMS Errors.ctl"/>
				<Item Name="FSU Step Motor.lvlib" Type="Library" URL="../Submodules/HIJ Motion/FSU Step Motor/FSU Step Motor.lvlib"/>
				<Item Name="Motor Actor.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Motor Actor/Motor Actor.lvlib"/>
				<Item Name="Motor Controller Actor.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Motor Contoller Actor/Motor Controller Actor.lvlib"/>
				<Item Name="Motor Controller IF.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Motor Controller IF/Motor Controller IF.lvlib"/>
			</Item>
			<Item Name="UI" Type="Folder">
				<Item Name="Angle Motor UI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Angle Motor UI/Angle Motor UI.lvlib"/>
				<Item Name="Calibration Config Dialog.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Calibration Config Dialog/Calibration Config Dialog.lvlib"/>
				<Item Name="EndContact Motor UI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/EndContact Motor UI/EndContact Motor UI.lvlib"/>
				<Item Name="Filter Slide.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Filter Slide/Filter Slide.lvlib"/>
				<Item Name="FilterWheelSets.lvlib" Type="Library" URL="../Submodules/HIJ Motion/FilterWheelSets/FilterWheelSets.lvlib"/>
				<Item Name="Motor UI Base.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Motor UI Base/Motor UI Base.lvlib"/>
				<Item Name="Motor UI IF.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Motor UI Interface/Motor UI IF.lvlib"/>
				<Item Name="Simple Motor Abs Pos UI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Simple Motor Abs Pos UI/Simple Motor Abs Pos UI.lvlib"/>
				<Item Name="Simple Motor GUI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Simple Motor GUI/Simple Motor GUI.lvlib"/>
				<Item Name="Slide Motor UI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Slide Motor UI/Slide Motor UI.lvlib"/>
				<Item Name="Special Position UI Base.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Special Position UI Base/Special Position UI Base.lvlib"/>
				<Item Name="Special Positions Dialog.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Special Positions Dialog/Special Positions Dialog.lvlib"/>
				<Item Name="Special Positions Motor UI.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Special Positions Motor UI/Special Positions Motor UI.lvlib"/>
			</Item>
			<Item Name="HIJ Motion App.lvlib" Type="Library" URL="../Submodules/HIJ Motion/HIJ Motion App/HIJ Motion App.lvlib"/>
		</Item>
		<Item Name="Active Strecker Motor Angle UI.lvlib" Type="Library" URL="../Active Strecker Motor Angle UI/Active Strecker Motor Angle UI.lvlib"/>
		<Item Name="Active Stretcher Motors.lvlib" Type="Library" URL="../Active Stretcher Motors/Active Stretcher Motors.lvlib"/>
		<Item Name="AS Graiting Angle In.lvlib" Type="Library" URL="../AS Grating Angle In UI/AS Graiting Angle In.lvlib"/>
		<Item Name="AS Separations Angle UI.lvlib" Type="Library" URL="../AS Separations Angle UI/AS Separations Angle UI.lvlib"/>
		<Item Name="Calculation Step by Step.vi" Type="VI" URL="../Calculation Step by Step.vi"/>
		<Item Name="test convert deg to rad.vi" Type="VI" URL="../test convert deg to rad.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVFontTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVFontTypeDef.ctl"/>
				<Item Name="LVMenuShortCut.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMenuShortCut.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVStringsAndValuesArrayTypeDef_U16.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef_U16.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Reply Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Reply Msg/Reply Msg.lvclass"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Configurable Motor Controller.lvlib" Type="Library" URL="../Submodules/HIJ Motion/Configurable Motor Controller/Configurable Motor Controller.lvlib"/>
			<Item Name="Decode_ID_Extra (SubVI).vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/CHIP_2101.llb/Decode_ID_Extra (SubVI).vi"/>
			<Item Name="Decode_ID_Owis.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/CHIP_2101.llb/Decode_ID_Owis.vi"/>
			<Item Name="decodeByte.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/decodeByte.vi"/>
			<Item Name="FTD2XX.dll" Type="Document" URL="FTD2XX.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Get Short LV Class Name.vi" Type="VI" URL="../Submodules/HIJ Motion/Get Short LV Class Name.vi"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="READ_ID_Owis.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/CHIP_2101.llb/READ_ID_Owis.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="USB_MASK.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_MASK.vi"/>
			<Item Name="USB_NET_CHIP_INFO.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_CHIP_INFO.vi"/>
			<Item Name="USB_NET_CURR_AREA.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_CURR_AREA.vi"/>
			<Item Name="USB_NET_DAT_SMS_VC0.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/USB_NET_DAT_SMS_VC0.vi"/>
			<Item Name="USB_NET_DAT_SMS_VC1.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/USB_NET_DAT_SMS_VC1.vi"/>
			<Item Name="USB_NET_E_POP.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_E_POP.vi"/>
			<Item Name="USB_NET_ID.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/CHIP_2101.llb/USB_NET_ID.vi"/>
			<Item Name="USB_NET_KONF_SMS.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_2101.llb/USB_NET_KONF_SMS.vi"/>
			<Item Name="USB_NET_MIC_STRING.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_MIC_STRING.vi"/>
			<Item Name="USB_NET_MOV_AREA.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_MOV_AREA.vi"/>
			<Item Name="USB_NET_RD_INI.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_RD_INI.vi"/>
			<Item Name="USB_NET_S_POP.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_S_POP.vi"/>
			<Item Name="USB_NET_SETUP.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_SETUP.vi"/>
			<Item Name="USB_NET_VELO_AREA.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_VELO_AREA.vi"/>
			<Item Name="USB_NET_VER_CHECK.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/ALL_2011.llb/USB_NET_VER_CHECK.vi"/>
			<Item Name="USB_NET_WR_INI.vi" Type="VI" URL="../Submodules/HIJ Motion/Bahrmann Utility/SMS_DEMO_V4.5_LV14_32bit/SMS_DEMO_2101.llb/USB_NET_WR_INI.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Active Stretcher Motors" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{4232E6AD-642D-4CC7-B7F2-21E459EF0943}</Property>
				<Property Name="App_INI_GUID" Type="Str">{09A84074-07D2-479D-B25D-12976B73CEFD}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="App_waitDebugging" Type="Bool">true</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{6210CD7B-E5C3-4A6F-B303-C26DDD5F9161}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Active Stretcher Motors</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/Active Stretcher Motors</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8639B556-F928-4EDD-BECC-43528C662F12}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">7</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">Active Stretcher Motors.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Active Stretcher Motors/Active Stretcher Motors.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/Active Stretcher Motors/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/Item[@Label='Dependencies' and @Type='Folder']/Active Stretcher Motors.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{3EBBE10F-6C87-420A-93F8-4428BF8ED7AB}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Active Stretcher Motors.lvlib/Launcher.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Item[@Label='Dependencies' and @Type='Folder']/FSU Bahrmann SMS/ftd2xx.dll</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/HIJ Motion/Controller/FSU SMS Actor.lvlib/Errors/FSU Motor Controller-errors.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/HIJ Motion/Controller/FSU SMS Actor.lvlib</Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/HIJ Motion/Controller/FSU SMS Actor.lvlib/FSU SMS Actor.lvclass</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_companyName" Type="Str">Friedrich-Schiller-Universität Jena</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Active Stretcher Motors Redesign nach M-G-M Wunsch.</Property>
				<Property Name="TgtF_internalName" Type="Str">Active Stretcher Motors</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2021 Friedrich-Schiller-Universität Jena</Property>
				<Property Name="TgtF_productName" Type="Str">Active Stretcher Motors</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{33752FCB-3357-43D3-8C74-7BA529BFEC1C}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Active Stretcher Motors.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
