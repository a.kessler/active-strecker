﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">AS Separations Angle UI.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../AS Separations Angle UI.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="AS Separation Angle Current Position for UI.vi" Type="VI" URL="../AS Separation Angle Current Position for UI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'R!=!!?!!"#(5&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*,GRW&lt;'FC)E&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*)%F',GRW9WRB=X-!!"Z"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633"*2C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!*5!;!!%!!!!"!"B$&gt;8*S:7ZU)&amp;.F='&amp;S982J&lt;WYA17ZH&lt;'5!!'J!=!!?!!"#(5&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*,GRW&lt;'FC)E&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*)%F',GRW9WRB=X-!!"V"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633"*2C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="AS Separation Angle Motion Finished.vi" Type="VI" URL="../AS Separation Angle Motion Finished.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'R!=!!?!!"#(5&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*,GRW&lt;'FC)E&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*)%F',GRW9WRB=X-!!"Z"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633"*2C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!;E"Q!"Y!!%)&gt;16-A5W6Q98*B&gt;'FP&lt;H-A17ZH&lt;'5A65EO&lt;(:M;7)C16-A5W6Q98*B&gt;'FP&lt;H-A17ZH&lt;'5A65EA359O&lt;(:D&lt;'&amp;T=Q!!(5&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*)%F')'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Update Separation Angle.vi" Type="VI" URL="../Update Separation Angle.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'R!=!!?!!"#(5&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*,GRW&lt;'FC)E&amp;4)&amp;.F='&amp;S982J&lt;WZT)%&amp;O:WRF)&amp;6*)%F',GRW9WRB=X-!!"Z"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633"*2C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(5!;!!%!!!!"!""4:8"B=G&amp;U;7^O)%&amp;O:WRF!!"K1(!!(A!!1BV"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633ZM&gt;GRJ9C*"5S"4:8"B=G&amp;U;7^O=S""&lt;G&gt;M:3"633"*2CZM&gt;G.M98.T!!!&gt;16-A5W6Q98*B&gt;'FP&lt;H-A17ZH&lt;'5A65EA359A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
